


import pandas as pd
import numpy as np

def data_processing(path):
    '''

    :param path:
    :return:
    '''
    print('start processing raw data\n')

    # cut time point exceed 2 mins if needed.

    df_main=pd.DataFrame(pd.read_csv(path),columns=['Time',
                                                    'Ask Price 1', 'Ask Size 1',
                                                    'Ask Price 2', 'Ask Size 2',
                                                    'Ask Price 3', 'Ask Size 3',
                                                    'Ask Price 4', 'Ask Size 4',
                                                    'Ask Price 5', 'Ask Size 5',
                                                    'Bid Price 1', 'Bid Size 1',
                                                    'Bid Price 2', 'Bid Size 2',
                                                    'Bid Price 3', 'Bid Size 3',
                                                    'Bid Price 4', 'Bid Size 4',
                                                    'Bid Price 5', 'Bid Size 5',
                                                    ])
    df_ask=df_main.iloc[:, :11]
    df_ask['My_Price'] = 0
    df_ask['My_Size'] = 0
    #df_ask['My_Rank'] = 6
    df_bid=df_main.drop(df_main.columns[1:11], index=1)
    df_bid['My_Price'] = 0
    df_bid['My_Size'] = 0
    return df_ask, df_bid


def add_new_order_row(dataframes,time_point,price,size):
    '''

    :param dataframes:
    :param time_point:
    :param price:
    :param size:
    :return:
    '''
    if time_point not in dataframes['Time']:
        dataframes_1 = dataframes[dataframes['Time']<time_point]
        insert_point = len(dataframes_1)
        # new_row.index=
        new_row = dataframes_1[-1]
        new_row['Time'] = time_point
        new_row['My_Price'] = price
        new_row['My_Size'] = size
        new_follow_row = dataframes[dataframes[insert_point:]]
        new_follow_row['My_Price'] = price
        new_follow_row['My_Size'] = size
        new_df = pd.concat([dataframes_1, new_row, new_follow_row]).reset_index(drop=True)
        return new_df
    else:
        dataframes[dataframes[dataframes['Time']==time_point].index:].My_Price=price
        dataframes[dataframes[dataframes['Time']==time_point].index:].My_Size=size
        return dataframes


def delete_old_my_order(dataframes,time_point):
    '''

    :param dataframes:
    :param time_point:
    :return:
    '''
    dataframes[dataframes[dataframes['Time'] == time_point].index:].My_Price = 0
    dataframes[dataframes[dataframes['Time'] == time_point].index:].My_Size = 0
    return dataframes


def order_the_price(dataframes,time_point):
    # this part is used for ranking prices when market order appear
    certain_row = dataframes[dataframes.Time == time_point]
    #!! may have multiple rows here.

    if dataframes=='df_ask':
        price_list = [
            str(certain_row[['Ask Price 1', 'Ask Price 2', 'Ask Price 3', 'Ask Price 4', 'Ask Price 5', 'My_Price']])]
        return price_list.sort(reverse=True)
        # decendence
    else:
        price_list = [
            str(certain_row[['Bid Price 1', 'Bid Price 2', 'Bid Price 3', 'Bid Price 4', 'Bid Price 5', 'My_Price']])]
        return price_list.sort()
        # acendence


def check_market_order_exsitence(dataframes,start_time,end_time):
    pass
    # compare neighboring two rows, check if the size decreased.
    # if true, remember the time, send indicators that market order appear ( return time_point).
    # attention: there might be lots of time points.


def act_for_market_order(dataframes,start_time,end_time):
    pass
    # check the existence of market_order
    # if exist, start ordering the price
    # decrease the sizes of different prices, even delete.
    #?? if the final price with sizes are higher than my price, my size is not change; otherwise, my size==0.
    #?? size is just the practical size (sum of sizes of the whole following price).

    # calculate the cost/reward and the rest size of my order.


def solu_every_2_minutes():
    pass
    # add 5 times of my order
    # react to the market order
    # reserve the best solution (state, action, price) under different 2 minutes.


def final_solus():
    pass
    # every 2 minute, find the best action for these two minutes.
    # every 2 minute, we can get a good action. delete the repeated part and return.





